# Target Audience

This specification is intended to meet a variety of needs, depending on their level of comfort with the material involved.
In order of precedence, the goal of this specification is to meet the needs of:

 - Node Developers
 - Wallet Developers
	 - Provide all the details necessary to write a node implementation from scratch.
	 - Provide a common space for documenting changes as they occur to continually represent a current model of the Nexa protocol
	 - Provide each node implementation with an opportunity to describe what they do different and why
 - Script Developers
	 - Provide all the details necessary to gain a complete understanding of script execution and the available scripting operations
	 - Provide examples of commonly used scripts and an explanation of the patterns with which they are created 
 - Service Providers
	 - Provide information on how to create and submit transactions to the network
	 - Provide information on how to collect and parse information about the state of the blockchain and network
 - Interested Non-Technical Parties
	 - Provide a high-level description of the major terms and concepts required to understand how Nexa operates and its current operational state
	 - Provide insight into how Nexa differs from Bitcoin Cash and from Bitcoin Core
