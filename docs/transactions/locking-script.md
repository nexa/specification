# Locking Script

A locking script is a [Script](../script/1script.md) that is used dictate how funds can be spent in the future.
Locking scripts do this by defining a set of operations that must end in a successful script execution.
The rest of the script execution is to be provided in an [unlocking script](unlocking-script.md) in a future transaction's input.
Every locking script is executed after the unlocking script.
If the unlocking script were run second, it could end by removing any values currently on the stack and replacing it with a success value.
For more information about how script execution works, see [Script](../script/1script.md).

## Standard Scripts

The below locking script formats are regarded as "standard" on the Bitcoin Cash network and, as a result, are the three forms of transactions that will be relayed among nodes for mining.
It is possible to have transactions without these standard locking script formats mined into a block, but such transactions are at a disadvantage due since they will not be as widely propagated.

### Pay to Public Key Hash (P2PKH)

Pay to Public Key Hash is a widely used standard locking script format, that works similarly to P2PK but instead of pushing the public key, it pushes a hash of the public key, commonly referred to as an [address](../addresses/address-types.md).
This heavily reduces the risks associated with a plain P2PK script as the hashing algorithms used provide a considerable barrier to determining the public key a priori.
To spend an output locked with this type of script, the unlocking script is expected to push a signature and then the public key corresponding to the private key that created the signature.
If that public key hashes to the expected address, and the signature is valid, the output is allowed to be spent.

| Operation | Description |
|--|--|
| [OP_DUP](../script/1script.md#stack) | Copy the value at the top of the stack (public key of the recipient). |
| [OP_HASH160](../script/1script.md#cryptography) | Perform a SHA-256 then a RIPEMD-160 on the copied value. |
| [OP_DATA_X](../script/op-codes/op-data-x.md) (20 bytes) | Push the expected 20 byte address. |
| [OP_EQUALVERIFY](../script/1script.md#bitwise) | Verify that the hash of the copied value matches the expected hash that was pushed. |
| [OP_CHECKSIG](../script/1script.md#cryptography) | Verify that the stack now contains only a public key (which was duplicated, hashed, and checked against the expected value) and a signature and verify that the signature is valid for that public key. |

### Multisig (P2MS)

Multiple-signature, or multisig, scripts provide a mechanism to have multiple private keys coordinate with spending funds.
For example, three people could share funds and require that for some transactions any one of them could spend it while, for others, two of them would need to agree, and for others still, all three people would need to agree to spend the funds.
Each party's public key is included in the locking script along with the required number of signatures (i.e. from above, 1, 2, and 3, respectively).

An unlocking script is therefore expected to provide the required number of signatures which are then checked against the list of public keys.  If a sufficient number of valid signatures are provided, the output is allowed to be spent.

These are also referred to as "bare multisig" scripts to disambiguate them from P2SH multisig scripts (see [Multisignature](../cryptography/multisignature.md)).

| Operation | Description |
|--|--|
| [OP_X](../script/op-codes/op-x.md) | Push the number of parties required to provide signatures. |
| 1 or more [OP_DATA_X](../script/op-codes/op-data-x.md) (public key) | Push 1 or more public keys, indicating all of the parties that could provide signatures. |
| [OP_X](../script/op-codes/op-x.md) | The total number of parties added (i.e. the number of public keys pushed). |
| [OP_CHECKMULTISIG](../script/1script.md#cryptography) | Check for signatures matching the number of required parties, verify that they correspond to permitted public keys, and that the signatures are valid. |

NOTE: due to a historical bug, the locking script must push an additional value before the signatures.  Traditionally this is done via [OP_0](../script/op-codes/op-x.md).  The value is not used but is popped off of the stack by the OP_CHECKMULTISIG at the end of the locking script.

### Data Output

Data Output scripts are used to create outputs that are not spendable but instead are used purely to add data to a transaction.
They are made provably unspendable by immediately failing script execution.
As such, outputs locked with data scripts generally have zero satoshis associated with them.

| Operation | Description |
|--|--|
| [OP_RETURN](../script/1script.md#flow-control) | Fail execution immediately. |
| Data Pushes | Data may optionally be added as a series of push operations.  However, these push operations are not executed and thus can push whatever arbitrary data is desired by the script creator. |
