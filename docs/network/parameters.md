# Network Parameters

The P2P protocol runs over TCP.

The RPC protocol runs over JSON-RPC over HTTP over TCP.

This table describes the relevant default TCP/IP ports:

| NEXA PORTS   | Mainnet | Testnet | Regtest |
| -----------  | ------- | ------- | ------- |
| P2P          | 7228    | 7230    | 18444   |
| RPC          | 7227    | 7229    | 18332   |

