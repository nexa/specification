# Response: Pong ("pong")

Connection keep-alive, "aliveness" and latency discovery.  This message is sent in response to a [`ping`](ping.md) message.

## Message Format

| Field | Length | Format | Description |
|--|--|--|--|
| nonce | 8 bytes | unsigned 64 bit integer<sup>[(LE)](../../misc/endian.md#little-endian)</sup>  | The value provided by the `ping` message.
