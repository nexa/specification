# Request: Send Headers (“sendheaders”)

Requests that new blocks are sent to the sender as a [`header`](headers.md) message instead of as a block hash [`inv`](inv.md) message.

## Message Format
This message has no contents.
