# NFT Categories

This is a list of NFT categories/subcategories that are in use in Nexa.  Please use an existing category, rather than a synonym.  Or propose to add a category/subcategory.  There is a much higher bar to adding a category verses a subcategory (and note that all current applications will not understand your new category so will likely place your NFT in the default category "NFT".

Note that these categories do not control any NFT functionality.  Their intention is simply to allow applications to properly organize a user's NFTs to make management of many NFTs simpler.  Therefore categories should be chosen to reflect how people should think of your NFT.  For example, a video game accomplishment token should be categorized as NFT.badge.[game name], *even if* it also allows access to a special discussion forum (and therefore is also an "access card") for advanced players.

Categories are case insensitive for algorithmic purposes.  They should be lower case with spaces unless they are an acronym or a proper noun (i.e. do not capitalize based on syntax).  Categories may contain any UTF-8 character EXCEPT period ".".  That is the category/subcategory separator.

Do NOT repeat the NFT name or title in the deepest subcategory.

## "coin"
*Crypto assets, such as stable coins,  hypothecated (wrapped) fiat, or cryptocurrencies*

## "NFT"
*nonfungible / semifungible tokens*

### Badges
*Badges are small tokens of accomplishment usually associated with digital activities.  For the traditional "badge" (e.g. access card), see "identity.access card".*

#### Format
NFT.badge.[application]
#### Existing categories
* NFT.badge.Wally Wallet

## "ticket"
*ticket to an event.  A "ticket" provides access to a particular event at a particular time.  It expires.  For access that is not related to a particular event (like a company badge), see "identity.access card".*

### Format
ticket.[event type]

## "identity"
*Anything to do with identifying the holder or providing the holder with services like access to a facility*

### identity.access card


## "coupon"
*get a discount from some merchant*


## "security"
*stock/bond/options*  

## "item"
*ownership of an item (that is typically but not necessarily physical) item such as land, gold, or an electronically readable book*

### Format

item.[type]
