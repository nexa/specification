# OP_BIN2BIGNUM

*Convert a byte string to a big number*  


## Syntax and Stack  

*D* **OP_BIN2BIGNUM** => *B* <sup>*[?](../opcodesyntax.md)*</sup>  

- *D*: The data to be converted to a BigNum
- *B*: The BigNum
#### Binary representation
OP_BIN2BIGNUM is defined as the single byte 0xec.

## Operation

D is parsed as a [little-endian sign-magnitude encoded number](LE_sign_mag_encoding.md) into a BigNum.  This magnitude of this  BigNum is then limited by executing a sign-preserving-modulo [BMD](../bignum_modulo_divisor.md) operation, and the result is pushed to the stack as B. **[BIN2BIGNUM.O1]**

Note, this binary format is compatible with ScriptNum, allowing the existing 32-bit script numbers to be easily converted to BigNums. **[BIN2BIGNUM.O2]**

If *D* is already a BigNum type, this opcode executes a sign-preserving-modulo [BMD](../bignum_modulo_divisor.md) operation, and the result is pushed to the stack as B. **[BIN2BIGNUM.O3]**  This behavior is equivalent to losslessly converting D to a binary representation and then converting it back to a BigNum.

## Limits

Although the largest BigNum can be 513 bytes, up to 520 byte objects are possible on the stack.  Numbers with encodings up to the maximum stack size MUST be accepted, but since the modulo occurs as part of this operation and the BMD cannot exceed 4096+1 sign bits , the number pushed onto the stack MUST fit into 4096+1 sign bits. **[BIN2BIGNUM.L1]**
