# Base58check encoding

Base58 encoding is *strongly* discouraged in Nexa.  You should not expect base58 addresses to be interoperable with arbitrary Nexa software.
