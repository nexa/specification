---
title: Home
---

# Nexa Blockchain Protocol and Consensus Specifications
Last updated: 2023 May 9

## Overview  

Nexa is a decentralized cryptocurrency with a public distributed ledger.
Transactions are validated and transmitted over a peer-to-peer (P2P) overlay (over TCP-IP) network, and stored in the [blockchain](blockchain/blockchain.md).
Nexa utilizes the Proof-of-Work timestamping and consensus scheme with a hash function that is a combination of elliptic curve multiplication and SHA-256, targeting a 2-minute block time.
It utilizes the secp256k1 parameters with the Schnorr algorithm for digital signatures.
The ticker symbol for Nexa is NEXA (or NEX if you must have 3 letters).

The Nexa consensus protocol is a modification of Bitcoin Cash, which is a modification of Bitcoin.  This documentation attempts to cover the full Nexa protocol, by leveraging previously written documentation for Bitcoin Cash.  Therefore you may occasionally see references to Bitcoin Cash that are no longer applicable.  Please notify us on [our issue tracker](https://gitlab.com/nexa/specification/-/issues) if you discover a problem.

[Style Guide](home/style-guide.md) -- [Contributors](home/contributors.md) -- [Target Audience](home/target-audience.md) <!-- -- [Project History](home/project-history.md) -->

[CWIK Markdown Cheat Sheet](home/cwikMarkdownCheatSheet.md)  

## Design  

``` mermaid
graph RL

  subgraph b0 [Block N-1]
  end

  subgraph b1 [Block N]
    hs1(Hash)
    ph1(Prev_Hash)
    ts1(Timestamp)
    mr1(Merkle Root)
  end

  subgraph b1_t [Merkle Tree]
    Hash0
    Hash1
    Hash2
    Hash3
    Hash01
    Hash23
    mr(Root)
  end

  subgraph b2[Block N+1]
  end

  classDef block fill: #AAA
  class b0,b1,b2 block

  b2 --> hs1
  ph1 --> b0

  mr --> mr1
  Hash01 --> mr
  Hash23 --> mr
  Tx0 --> Hash0
  Tx1 --> Hash1
  Tx2 --> Hash2
  Tx3 --> Hash3
  Hash0 --> Hash01
  Hash1 --> Hash01
  Hash2 --> Hash23
  Hash3 --> Hash23
```

Nexa operates on the blockchain which is replicated among the nodes in the Nexa overlay network.
[Transactions](transactions/1transaction.md) are submitted to network nodes, which will [validate the transaction](transactions/validation/transaction-validation.md) against the transaction history in the blockchain.
Once transactions are considered valid, they will be grouped into blocks through [Merkle Trees](blocks/merkle-tree.md).
Through rigorous hash computation, blocks can be mined into the blockchain by the network nodes and are subsequently broadcast to the network.

The blockchain serves as the public ledger for the nexa cryptocurrency and participant-defined tokens.
It consists of a tree of blocks, where each block references its parent by cryptographic hash. Of this tree, one single chain of blocks probabilistically contains overwhelmingly more cumulative hashing work than any other chain.
This chain is called the "main chain" and its history, from genesis block to tip, defines the current state of the ledger, achieving consensus about what transactions are included in the blockchain and ledger.

Due to the characteristics of hash functions, the content of any block cannot be altered without changes to all its subsequent blocks.
The time and computation intensity of such change increases as the blockchain is extended with new blocks.

Therefore, the transaction history in the blockchain at an arbitrary point in the past can be considered probabilistically immutable even with public access, with the probability of immutability increasing the deeper in the chain the transaction is found.

The transactions on Nexa are pseudonymous.
The blockchain does not keep records of coin ownerships for users.
Instead, each transaction refers to the unspent outputs of previous transactions.
The outputs of the transaction is then locked through [locking scripts](transactions/locking-script.md) and whoever holds the correct [unlocking scripts](transactions/unlocking-script.md) can use the outputs in their future transactions.

The Nexa network is an ad-hoc decentralized network of volunteers, in which transactions are transmitted and validated.
Messages on the network are usually broadcast on a best-effort basis.
