# Overview

Simplified Payment Verification (SPV) is an alternative method for clients to verify [transactions](../transactions/1transaction.md) without access to the entire [blockchain](../blockchain/blockchain.md).
Clients that implement SPV typically do not store, validate, or broadcast most of the [blocks](../blocks/block.md) on the chain, and thus, significantly reduce the resources needed for operation.
In contrast, nodes that store the full blockchain are often referred to as full nodes.

## Theory

The validity of transactions can not only be verified through checking the transactions against the public ledger, but can also be demonstrated through the facts that they have been accepted by the network and admitted into the blockchain.
SPV relies on the latter method mentioned and verifies transactions by querying other network nodes for de facto proof that the blocks containing the transactions are valid.
The clients can keep their own copy of the [block header](../blocks/block.md#block-header chain and verify that the blocks referred are included in the blockchain.
The clients can further increase their confidence for the validity of the said transactions, as the confirmations go up due to the increased difficulty to forge the additional blocks and the decreased likelihood of a [blockchain reorganization](../blockchain/blockchain.md#blockchain-reorganization).

## Design

The support for SPV was proposed in [BIP-37](../forks/bip-0037.md).
Clients that wish to operate in SPV mode should utilize the [`filterload`](../network/messages/filterload.md) and [`filteradd`](../network/messages/filteradd.md) messages to request the remote peers to install and add content to the bloom filter on its connection with the client.
A Bloom filter is a probabilistic data structure which allows for testing set membership - they can have false positives but not false negatives.
Upon receipt of these messages, the remote peers are expected to only announce transactions selected by the filter, with the matching algorithm proposed in [BIP-37](../forks/bip-0037.md).
Nodes that accept a filterload message are expected to only send [inventory messages](../network/messages/inv.md) containing transactions that match the client's bloomfilter, or blocks that contain transactions that match the client's Bloom filter.
When blocks are requested by the client, they are sent not as [`block`](../network/messages/block.md) messages, but as [`merkle block`](../network/messages/merkleblock.md) messages, which contain the partial [merkle trees](../blocks/merkle-tree.md) of the blocks as the proof of the inclusion for the transactions announced.
The transactions can be transmitted to the clients normally, via [`tx`](../network/messages/tx.md) messages.
The clients can utilize the [`filterclear`](../network/messages/filterclear.md) message to clear the Bloom filter on the connection, to revert the connection to its typical state.

## Advantages and Disadvantages

SPV allows the clients to only receive, store, and validate parts of the blockchain without hampering their ability to verify the transactions they care about, and thus, significantly reduce the storage space, computational power and network traffic needed for operation.
Such characteristics make them ideal for environments with meager performance, or on metered networks, such as mobile wallets.

However, SPV method typically requires more confirmation times to build up confidence for transactions, and in the event of an attack, such clients will not be able to detect the fabricated transactions as long as the attacker can overpower the network.
In addition, SPV leaks information about the addresses a client is interested in to the nodes it installs Bloom filters into.  This information leak can be mitigated by providing a less precise Bloom filter.  However, this requires that more data be transmitted from full node to client.

Full nodes can also execute a denial-of-service attack on clients by not reporting transactions that match the filter.  SPV clients can mitigate this by talking to several nodes, or to "lightly" trusted nodes (not trusted with private keys, but trusted to report all transactions).
