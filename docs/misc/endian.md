# Endian

[Endianness](https://en.wikipedia.org/wiki/Endianness) refers to the implies order of bytes for a given value.

Values are generally either [big-endian](#big-endian), with the most significant bytes come first, or [little-endian](#little-endian), with the least significant values first.  Nexa used a mix of little- and big-endian values, with little-endian byte order generally being used for integers.


## Big-Endian

Big-endian byte ordering, also referred to as network byte ordering, is used in many places in the Nexa protocol.  While not always called out explicitly in this specification, it is called out when readers might be inclined to expect little-endian byte ordering.

In big-endian byte ordering, the most significant byte is first, following the second-most-significant, and so on.
This is what many would consider the "natural" order in most circumstances.

For example, the value 5,000, when represented in big-endian format, would be the byte 0x13 followed by 0x88, while 5,001 would be the byte 0x13 followed by 0x89.
Contrast this with [little-endian](#little-endian).

For information, see [Endianness](https://en.wikipedia.org/wiki/Endianness).


## Little-Endian

Little-endian byte ordering is widely used throughout the Nexa protocol, particularly for integers.

In little-endian byte ordering, the least significant byte is first, following the second-least-significant, and so on.
This is often the opposite of what people consider "natural" for numbers, despite being fairly common in computer hardware.

For example, the value 5,000, when represented in little-endian format, would be the byte 0x88 followed by 0x13, while 5,001 would be the byte 0x89 followed by 0x13.
Contrast this with [big-endian](#big-endian).

For information, see [Endianness](https://en.wikipedia.org/wiki/Endianness).
